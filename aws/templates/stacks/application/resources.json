{
  "AutoScalingGroupBase": {
    "Type": "AWS::AutoScaling::AutoScalingGroup",
    "Description": "Auto scaling group base instance",
    "Properties": {
      "AvailabilityZones": [
        { "Fn::Select": [0, { "Ref": "AvailabilityZones" }] }
      ],
      "Cooldown":        "500",
      "DesiredCapacity": { "Ref": "InstanceCount" },
      "HealthCheckGracePeriod":  1000,
      "HealthCheckType": "EC2",
      "LaunchConfigurationName": {"Ref": "AutoScalingLaunchConfigurationBase" },
      "LoadBalancerNames": [
        { "Ref": "ElasticLoadBalancerBase" }
      ],
      "MaxSize": { "Ref": "AutoScalingGroupMaxSize" },
      "MinSize": { "Ref": "AutoScalingGroupMinSize" },
      "Tags": [
        {
          "Key":   "AmiVersion",
          "Value": { "Ref": "InstanceAmiVersion" },
          "PropagateAtLaunch": true
        },
        {
          "Key":   "Environment",
          "Value": { "Ref": "Environment" },
          "PropagateAtLaunch": true
        },
        {
          "Key":   "Name",
          "Value": {
            "Fn::Join": [
              "-", [
                {"Ref": "AppName"},
                { "Ref": "Environment" },
                { "Ref": "VersionString" }
              ]
            ]
          },
          "PropagateAtLaunch": true
        },
        {
          "Key":   "Role",
          "Value": { "Ref": "InstanceBaseRole" },
          "PropagateAtLaunch": true
        },
        {
          "Key":   "Version",
          "Value": { "Ref": "Version" },
          "PropagateAtLaunch": true
        }
      ],
      "VPCZoneIdentifier": [
        { "Ref": "SubnetPrivate" }
      ]
    },
    "UpdatePolicy": {
      "AutoScalingRollingUpdate": {
        "MaxBatchSize":          "1",
        "MinInstancesInService": "1"
      }
    }
  },
  "AutoScalingGroupBasePolicyScaleDown": {
    "Type": "AWS::AutoScaling::ScalingPolicy",
    "Properties": {
      "AdjustmentType":       "ChangeInCapacity",
      "AutoScalingGroupName": { "Ref": "AutoScalingGroupBase" },
      "Cooldown":          "60",
      "ScalingAdjustment": "-1"
    }
  },
  "AutoScalingGroupBasePolicyScaleUp": {
    "Type": "AWS::AutoScaling::ScalingPolicy",
    "Properties": {
      "AdjustmentType":       "ChangeInCapacity",
      "AutoScalingGroupName": { "Ref": "AutoScalingGroupBase" },
      "Cooldown":          "60",
      "ScalingAdjustment": "1"
    }
  },
  "AutoScalingLaunchConfigurationBase": {
    "Type": "AWS::AutoScaling::LaunchConfiguration",
    "Properties" : {
      "AssociatePublicIpAddress": false,
      "ImageId": {
        "Fn::FindInMap": [
          "CoreOSInstanceAmiId",
          { "Ref" : "AWS::Region" },
          "AMI"
        ]
      },
      "InstanceMonitoring": true,
      "InstanceType":       { "Ref": "InstanceTypeBase" },
      "KeyName":            { "Ref": "KeyName" },
      "SecurityGroups" : [
        { "Ref": "CoreOsSecurityGroup" },
        { "Ref": "CoreOsSecurityGroupSource" },
        { "Ref": "SecurityGroupHttp" }
      ],
      "UserData": {
        "Fn::Base64": {
          "Fn::Join": ["",
            [
              "#cloud-config\n\n",
              "write_files:\n",
              "  - path: /home/core/.dockercfg\n",
              "    permissions: '0644'\n",
              "    owner: core:core\n",
              "    content: |\n",
              "      {\n",
              "        \"docker.onmyblock.com:443\": {\n",
              "          \"auth\": \"amVua2luczpvbm15YmxvY2tlcnM=\",\n",
              "          \"email\": \"development@onmyblock.com\"\n",
              "        }\n",
              "      }\n",
              "\n",
              "  - path: /etc/profile.d/etcdctl.sh\n",
              "    permissions: 0644\n",
              "    owner: core\n",
              "    content: |\n",
              "      # configure etcdctl to work with our etcd servers set abo\n",
              "      export ETCDCTL_PEERS=\"http://", { "Ref": "ElasticLoadBalancerBaseDNSName" }, ":4001\"\n",
              "  - path: /etc/profile.d/fleetctl.sh\n",
              "    permissions: '0644'\n",
              "    owner: core\n",
              "    content: |\n",
              "      # configure fleetctl to work with our etcd servers seabove\n",
              "      export FLEETCTL_ENDPOINT=unix:///var/run/fleet.sock\n",
              "      export FLEETCTL_EXPERIMENTAL_API=true\n",
              "coreos:\n",
              "  update:\n",
              "    reboot-strategy: off\n",
              "  fleet:\n",
              "    public-ip: $", { "Ref": "CoreOsAdvertisedIPAddress" }, "_ipv4:4001\n",
              "    etcd_servers: \"http://", { "Ref": "ElasticLoadBalancerBaseDNSName" }, ":4001\"\n",
              "    metadata: \"env=", { "Ref": "Environment" },",role=", { "Ref": "AppName" },",region=", { "Ref": "AWS::Region" }, ",\"\n",
              "  units:\n",
              "    - name: var-lib-docker.mount\n",
              "      command: start\n",
              "      content: |\n",
              "        [Unit]\n",
              "        Before=docker.service\n",
              "        [Mount]\n",
              "        What=/dev/xvdb\n",
              "        Where=/var/lib/docker\n",
              "        Type=ext4\n",
              "        Options=nodev,commit=600\n",
              "    - name: fleet.service\n",
              "      command: start\n",
              "    - name: fleet.socket\n",
              "      command: start\n",
              "      drop-ins:\n",
              "      - name: 30-ListenStream.conf\n",
              "        content: |\n",
              "         [Socket]\n",
              "         ListenStream=127.0.0.1:49153\n",
              "    - name: maestro.service\n",
              "      command: start\n",
              "      enable: true\n",
              "      content: |\n",
              "        [Unit]\n",
              "        Description=Maestro!!",                                    
              "        After=docker.service\n",
              "        Requires=docker.service\n",
              "\n",
              "        [Service]\n",
              "        User=core\n",
              "        TimeoutStartSec=0\n",
              "        EnvironmentFile=/etc/environment\n",
              "        ExecStartPre=-/usr/bin/docker kill maestro\n",
              "        ExecStartPre=-/usr/bin/docker rm maestro\n",
              "        ExecStartPre=/usr/bin/docker pull docker.onmyblock.com:443/maestro:latest\n",
              "        ExecStart=/usr/bin/docker run ",
                       " --name maestro ",
                       " --net=host",
                       " -v /opt/:/opt/",
                       " docker.onmyblock.com:443/maestro:latest",
                       " ruby /app/maestro.rb",
                       " run --role ",
                       { "Ref": "AppName" },
                       " -c /app/maestro.cfg",
                       " --ec2data_enable ",
                       " 1",
                       " --etcdmaster ",
                       { "Ref": "ElasticLoadBalancerBaseDNSName" },
                       " --machineid %m\n",
              "        ExecStop=/usr/bin/docker stop maestro\n",
              "\n",
              "        [Install]\n",
              "        WantedBy=multi-user.target\n"
            ]
          ]
        }
      }
    }
  },
  "CloudWatchAlarmCPUHigh": {
    "Type": "AWS::CloudWatch::Alarm",
    "Properties": {
      "AlarmActions": [
        { "Ref": "AutoScalingGroupBasePolicyScaleUp" }
      ],
      "AlarmDescription":   "Scale-up if CPU > 90% for 10 minutes",
      "AlarmName": {
        "Fn::Join": [
          "-", [
            "cpuhigh",
            { "Ref": "AppName" },
            { "Ref": "Environment" },
            { "Ref": "VersionString" }
          ]
        ]
      },
      "ComparisonOperator": "GreaterThanThreshold",
      "Dimensions": [
        {
          "Name":  "AutoScalingGroupName",
          "Value": { "Ref": "AutoScalingGroupBase" }
        }
      ],
      "EvaluationPeriods": "2",
      "MetricName":        "CPUUtilization",
      "Namespace":         "AWS/EC2",
      "Period":            "300",
      "Statistic":         "Average",
      "Threshold":         "90"
    }
  },
  "CloudWatchAlarmCPULow": {
    "Type": "AWS::CloudWatch::Alarm",
    "Properties": {
      "AlarmActions": [
        { "Ref": "AutoScalingGroupBasePolicyScaleDown" }
      ],
      "AlarmDescription":   "Scale-down if CPU is less than 70% for 10 minutes",
      "AlarmName": {
        "Fn::Join": [
          "-", [
            "cpulow",
            { "Ref": "AppName" },
            { "Ref": "Environment" },
            { "Ref": "VersionString" }
          ]
        ]
      },
      "ComparisonOperator": "LessThanThreshold",
      "Dimensions": [
        {
          "Name":  "AutoScalingGroupName",
          "Value": { "Ref": "AutoScalingGroupBase" }
        }
      ],
      "EvaluationPeriods": "2",
      "MetricName":        "CPUUtilization",
      "Namespace":         "AWS/EC2",
      "Period":            "300",
      "Statistic":         "Average",
      "Threshold":         "70"
    }
  },
  "ElasticLoadBalancerBase": {
    "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
    "Description": "Elastic load balancer for auto scaling group base",
    "Properties": {
      "HealthCheck": {
        "HealthyThreshold":   "3",
        "Interval":           "10",
        "Target":             "HTTP:80/health",
        "Timeout":            "5",
        "UnhealthyThreshold": "5"
      },
      "LoadBalancerName": {
        "Fn::Join": [
          "-", [
            { "Ref": "AppName" },
            { "Ref": "Environment" },
            { "Ref": "VersionString" }
          ]
        ]
      },
      "Listeners": [
        {
          "InstancePort":     "80",
          "InstanceProtocol": "http",
          "LoadBalancerPort": "80",
          "Protocol":         "http"
        }
      ],
      "SecurityGroups": [
        { "Ref": "SecurityGroupHttp" }
      ],
      "Subnets": [
        { "Ref": "SubnetPublic" }
      ]
    }
  }
}
