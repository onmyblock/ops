# TODO
  - [ ] deal with cleanup when a failure occurs.
  - [ ] verify old stack drying out before decommission it.
  - [ ] verify that the app health check is testing the database.
  - [ ] Add automatic creation of sub zones NS records when creating new environment, so dns recursive searches would work for external DNS servers/clients.
  - [ ] Pass registry pass as a parameter and not hardcoded in the template.

# Deploying a new environment
When deploying a new environment we need to make sure to relate the sub domain zone created by aws_deploy.rb such that we could recursively search it while querying onyblock.com.

For example, say we need to have the following application in DNS 'users.dev.us-east-1.onmyblock.com', In order to accomplish recursion for this record, we need to do the following:

1. Create an 'NS' record for us-east-1.onmyblock.com under onmyblock.com, and specify the name servers that route53 automatically assigned to 'us-east-1.onmyblock.com'.
2. Create another 'NS' record for 'dev.us-east-1.onmyblock.com' in 'us-east-1.onmyblock.com', and specify the name servers that route 53 automatically assigned to 'dev.us-east-1.onmyblock.com'.

At this point recursive search for 'users.dev.us-east-1.onmyblock.com' will work correctly, in the future we'll add code that will deal with this automatically.

# Jenkins Pipeline

* Jenkins deploys applications to AWS from within an `onmyblock/aws` Docker container using `aws_deploy.rb`.
* Jenkins will persist variables between steps by writing them to the `$PARAMS` file.
* To deploy manually, set AWS credentials, default variables, and `$PARAMS` before running each command in sequence.
* Manual deploys should generally be avoided because we use CI.

AWS credentials:

```
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
```

Default variables:

```
ASG_MAX_SIZE=4
ASG_MIN_SIZE=2
AVAILABILITY_ZONES=us-west-2a
ENVIRONMENT=staging
KEY_NAME=aws_omb_us_west_2
REGION=us-west-2
```

Example `$PARAMS`:

```
APP_NAME=users
APP_VERSION=1.0.1
COMMIT=f834133
DOCKER_APPLICATION_IMAGE_TAG=docker.onmyblock.com:443/users-package:f834133
OPS=/Users/devin/Dropbox/git/ops
```

### 1) get_current_app_version

Get the current application stack version so we know which one to delete at the end.

* Exit with the version of the currently deployed application stack.

```
ruby aws_deploy.rb -a get_current_app_version --app_name=$APP_NAME --environment=$ENVIRONMENT --regions=$REGION
```

### 2) create_app_stack

Create application stack and register the ELB in Route53 with weight: 0.

* Exit 0 if stack is created and the Route53 record set is successfully created or updated.
* Exit 1 if either stack creation or record set update fails.

```
docker run \
  -v $OPS/aws:/opt \
  -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
  -e APP_NAME=$APP_NAME \
  -e ASG_MAX_SIZE=$ASG_MAX_SIZE \
  -e ASG_MIN_SIZE=$ASG_MIN_SIZE \
  -e AVAILABILITY_ZONES="$AVAILABILITY_ZONES" \
  -e DB_REPLICATION=$DB_REPLICATION \
  -e DOCKER_APPLICATION_IMAGE_TAG=$DOCKER_APPLICATION_IMAGE_TAG \
  -e ENVIRONMENT=$ENVIRONMENT \
  -e KEY_NAME=$KEY_NAME \
  -e REGION="$REGION" \
  -e APP_VERSION=$APP_VERSION \
  onmyblock/aws /bin/sh -c 'cd /opt; /root/.rbenv/shims/ruby \
  aws_deploy.rb \
    -a create_app_stack_and_register_elb \
    --app_name=$APP_NAME \
    --asg_max_size=$ASG_MAX_SIZE \
    --asg_min_size=$ASG_MIN_SIZE \
    --availability_zones=$AVAILABILITY_ZONES \
    --db_replication=$DB_REPLICATION \
    --docker_application_image_tag=$DOCKER_APPLICATION_IMAGE_TAG \
    --environment=$ENVIRONMENT \
    --key_name=$KEY_NAME \
    --regions=$REGION \
    --template_dir=/opt \
    --version=$APP_VERSION'
```

Writes these `$PARAMS`:

```
APP_URL=
ELB_DNS_NAME=
ELB_NAME=
ZONE=
```

### 3) ensure_elb_in_service

Check ELB instance health every 10 seconds.

* Exit 0 if all instances are in service.
* Exit 1 if some instances are still not in service after 5 minutes.

```
docker run \
  -v $OPS/aws:/opt \
  -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
  -e ELB_NAME=$ELB_NAME \
  -e ENVIRONMENT=$ENVIRONMENT \
  -e REGION=$REGION \
  onmyblock/aws /bin/sh -c 'cd /opt; /root/.rbenv/shims/ruby \
  aws_deploy.rb \
    -a ensure_elb_in_service \
    --app_name=$APP_NAME \
    --environment=$ENVIRONMENT \
    --regions=$REGION \
    --version=$APP_VERSION'
```

### 4) switch_traffic

Update the existing Route53 record set with weight: 100 for the stack that has the version of --version_new, and will set weight to 0 on the stack that is running --version_old.

* Exit 0 if record set update is successful.
* Exit 1 if not successful.

```
docker run \
  -v $OPS/aws:/opt \
  -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
  -e REGION=$REGION \
  -e AVAILABILITY_ZONES="$AVAILABILITY_ZONES" \
  -e APP_NAME=$APP_NAME \
  -e ENVIRONMENT=$ENVIRONMENT \
  -e VERSION_NEW=$VERSION_NEW \
  -e VERSION_OLD=$VERSION_OLD \
  onmyblock/aws /bin/sh -c 'cd /opt; /root/.rbenv/shims/ruby \
  aws_deploy.rb \
    -a switch_traffic \
    --regions=$REGION \
    --availability_zones=$AVAILABILITY_ZONES \
    --app_name=$APP_NAME \
    --environment=$ENVIRONMENT \
    --version_new=$VERSION_NEW \
    --version_old=$VERSION_OLD
```

### 5) ensure_app_heartbeat

Ensure the application's /health endpoint is responding 200 OK.

* Exit 0 if the response has a status code of 200.
* Exit 1 if the response has any other status code.

```
docker run \
  -v $OPS/aws:/opt \
  -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
  onmyblock/aws /bin/sh -c 'cd /opt; /root/.rbenv/shims/ruby \
  aws_deploy.rb \
    -a ensure_app_heartbeat \
    --app_url='$APP_URL''
```

### 6) delete_old_app_stack

Delete the stack for this application that was previously running in the same `$ENVIRONMENT` and `$REGION`.

* Exit 0 if stack deletion is successful.
* Exit 1 if not successful.

```
docker run \
  -v $OPS/aws:/opt \
  -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
  onmyblock/aws /bin/sh -c 'cd /opt; /root/.rbenv/shims/ruby \
  aws_deploy.rb \
    -a ensure_app_heartbeat \
    --app_url='$APP_URL' \
    --environment='$ENVIRONMENT' \
    --regions='"$REGION"''
```

# Stack CRUD

To run the script, run the following command:

    $ ruby cloudformation_stack.rb

### Parameters
Argument | Short | Description | Example
--- | --- | --- | ---
--action | -a | action for script | create, delete, update
--asg_max_size | n/a | autoscaling group max instance size | 4
--asg_min_size | n/a | autoscaling group min instance size | 2
--availability_zones | -z | availability zones | us-west-1a,us-west-1b
--cidr_block | -c | CIDR block for VPC | 10.1.0.0/16

- required parameters in all services: action (-a), environment (-e), region (-r), and version (-v)

### Create stack

    $ ruby cloudformation_stack.rb -a create -z us-east-1a -e dev -i ami-9eaa1cf6 -k aws_omb_us_east_1 -n ami-224dc94a -r us-east-1 -v 1.0.0

### Create autoscaling group

    $ ruby cloudformation_stack.rb -a create_autoscaling -e dev -i ami-98aa1cf0 -r us-east-1 -v 1.0.0

### Delete stack

    $ ruby cloudformation_stack.rb -a delete -e dev -r us-east-1 -v 1.0.0

### Delete autoscaling group

    $ ruby cloudformation_stack.rb -a delete_autoscaling -e dev -r us-east-1 -v 1.0.0

### List Stacks

    $ ruby cloudformation_stack.rb -a list -e dev -r us-east-1 -v 1.0.0

### Update stack

    $ ruby cloudformation_stack.rb -a update -e dev -i ami-98aa1cf0 -r us-east-1 -v 1.0.0

### Connect to RDS database

    $ psql -h basedev200.chelwdkbulm5.us-east-1.rds.amazonaws.com -p 5432 -U postgres
