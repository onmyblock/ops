require_relative "objects/all"
require_relative "services/all"
STDOUT.sync = true

options_handler = OptionsHandler.new
options_handler.convert_options

options = options_handler.options
action  = options[:action]
weight = 0
ttl = 300
app_name = options[:app_name]
environment = options[:environment]
version = options[:version]
# setting layer default to application
options[:layer] = "application"
options_handler.log_options
options_handler.set_default_options
#options_handler.validate_options



case action
when "get_current_app_version"
  cloudformation_service = CloudformationService.new(options)
  stacks = cloudformation_service.find_stacks_with_tags(
    {
      appname: app_name,
      environment: environment
    }
  )

  # Do not start the deploy if there is not at least one currently deployed
  # app stack in this region and environment
  if stacks.count > 0
    app_version = stacks.last.tags.detect { |tag| tag.key == "Version" }.value
    puts "app_version: #{app_version}"
    exit 0
  else
    puts "#{Time.now.utc}: [ERROR] - There are no currently deployed
          #{app_name} stacks."
    exit 1
  end
when "create_app_stack_and_register_elb"
  options[:action] = "create"
  template_service = TemplateService.new(options)
  if CreateService.new(options).create(options[:layer], nil, template_service)
    cloudformation_service = CloudformationService.new(options)
    route53_service = Route53Service.new(options)

    zone_name = cloudformation_service.zone_name
    zone_name += "." unless zone_name[-1] == "."
    app_fqdn = app_name + "." + zone_name

    stack = cloudformation_service.find_stack_with_tags(
      {
        appname: app_name,
        environment: environment,
        version: version
      }
    )

    elb_service   = LoadBalancerService.new(options)
    elb_base_name = elb_service.name_from_outputs(stack.outputs)
    elb_dns_name  = elb_service.dns_name_from_outputs(stack.outputs)
    elb_dns_name += "." unless elb_dns_name[-1] == "."
    hosted_zone = route53_service.find_hosted_zone(zone_name)
    # checking if we already have this zone hosted, if not create it.
    unless hosted_zone
      puts "#{Time.now}: creating zone #{hosted_zone}"
      hosted_zone = route53_service.create_hosted_zone(zone_name)
      change_info = route53_service.get_change(hosted_zone.data.change_info.id)
      route53_service.wait_until_change_insync(change_info)
      # we seem to need to wait for route 53 to actually have a zone ready even after the change set is 'INSYNC'
      sleep(10)
    end
    if route53_service.rrset_pointer_exists?(hosted_zone, app_fqdn, elb_dns_name, version)
      puts "#{Time.now}: #{app_fqdn} already have a pointer to #{elb_dns_name}, extiting."
      # TODO: clean up newly created stack.
      exit(1)
    else
      #Checking whether this is the first application stack, if so, set weight to 100.
      weight = 100 if route53_service.find_record_set(hosted_zone, app_fqdn).empty?
      puts "#{Time.now}: creating #{elb_dns_name} in #{app_fqdn} with a weight of #{weight}"
      response = route53_service.change_resource_record_set(hosted_zone, elb_dns_name, app_fqdn, "CREATE", "CNAME", version, weight)
    end
    change_info = route53_service.get_change(response.data.map(&:id).join(" "))
    route53_service.wait_until_change_insync(change_info)
  else
    exit 1
  end
when "ensure_elb_in_service"
  # options_handler.validate_options
  cloudformation_service = CloudformationService.new(options)
  elb_service = LoadBalancerService.new(options)
  stack = cloudformation_service.find_stack_with_tags(
    {
      appname: app_name,
      environment: environment,
      version: version
    }
  )

  elb_name = elb_service.name_from_outputs(stack.outputs)
  exit elb_service.wait_until_instances_in_service(elb_name)
when "switch_traffic"
  version_old = options[:version_old]
  version_new = options[:version_new]
  cloudformation_service = CloudformationService.new(options)
  zone_name = cloudformation_service.zone_name
  zone_name += "." unless zone_name[-1] == "."
  app_fqdn = app_name + "." + zone_name

  route53_service = Route53Service.new(options)
  hosted_zone = route53_service.find_hosted_zone(zone_name)
  response = route53_service.set_stack_weight(hosted_zone, app_fqdn, version_new, 100, ttl)
  exit (1) unless response
  puts "#{Time.now}: Activating version #{version_new} #{app_fqdn}"
  change_info = route53_service.get_change(response.change_info.id)
  route53_service.wait_until_change_insync(change_info)

  response = route53_service.set_stack_weight(hosted_zone, app_fqdn, version_old, 0, ttl)
  exit (1) unless response
  puts "#{Time.now}: Deactivating version #{version_old} #{app_fqdn}"
  change_info = route53_service.get_change(response.change_info.id)
  route53_service.wait_until_change_insync(change_info)

when "ensure_app_heartbeat"
  cloudformation_service = CloudformationService.new(options)
  zone_name = cloudformation_service.zone_name
  app_fqdn = app_name + "." + zone_name
  exit TestService.new(options).heartbeat(app_fqdn)
when "delete_app_stack"
  cloudformation_service = CloudformationService.new(options)
  stack = cloudformation_service.find_stack_with_tags(
    {
      appname: app_name,
      environment: environment,
      version: version
    }
  )
  if stack.nil?
    puts "#{Time.now}: so such stack."
    exit(1)
  end
  elb_service   = LoadBalancerService.new(options)
  elb_base_name = elb_service.name_from_outputs(stack.outputs)
  elb_dns_name  = elb_service.dns_name_from_outputs(stack.outputs)
  elb_dns_name += "." unless elb_dns_name[-1] == "."
  zone_name = cloudformation_service.zone_name
  zone_name += "." unless zone_name[-1] == "."
  app_fqdn = app_name + "." + zone_name

  route53_service = Route53Service.new(options)
  hosted_zone = route53_service.find_hosted_zone(zone_name)
  if route53_service.rrset_active?(hosted_zone, app_fqdn, version)
    puts "#{Time.now}: cannot delete active record set #{app_fqdn} with target #{elb_dns_name}, since it is in use."
    exit(1)
  else
    response = route53_service.change_resource_record_set(hosted_zone, elb_dns_name, app_fqdn, "DELETE", "CNAME", version, weight)
    unless response
      puts "#{Time.now}: warning: skipping dns delete #{app_fqdn} with target #{elb_dns_name} running version #{version}, cannot find record."
    else
      puts "#{Time.now}: deleted #{app_fqdn} with target #{elb_dns_name}, version #{version}, and weight of #{weight}, waiting for change to get in sync.."
      change_info = route53_service.get_change(response.data.map(&:id).join(" "))
      route53_service.wait_until_change_insync(change_info)
    end
    delete = DeleteService.new(options).delete app_name, version, options[:layer]
    puts "#{Time.now}: done"
  end
else
  exit 1
end
