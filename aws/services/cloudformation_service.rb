require_relative "service"

class CloudformationService < Service
  def initialize(opts = {})
    super
    @service = Aws::CloudFormation
  end

  def app_name
    options[:app_name]
  end

  def dependencies
    {
      # disable storage layer
      # application:     ["storage", "shared"],
      application:     ["shared", "etcd_master", "storage"],
      docker_registry: ["shared", "network"],
      jenkins:         ["shared", "network"],
      network:         [],
      etcd_master:     ["shared", "network"],
      shared:          ["network"],
      storage:         ["shared"]
    }
  end

  def dependencies_for_stack(name)
    dependencies[name.to_sym]
  end

  def find_stack(name)
    service_resource.stack name
  end

  def find_stack_with_tags(opts = {})
    stack = nil
    stacks.each do |stk|
      break if stack
      stack = stk if matching_tags? stk, opts
    end
    stack
  end

  def find_stacks_with_tags(opts = {})
    stacks.select { |stk| matching_tags? stk, opts }
  end

  def layer_name(name = "stack")
    layer = if name == "application"
      "app-#{app_name}"
    else
      name.split("_").join("-")
    end
  end

  def matching_tags?(stack, opts = {})
    match = true
    tags  = {}
    stack.tags.each do |tag|
      tags[tag[:key].downcase] = tag[:value]
    end
    opts.each do |key, value|
      break unless match
      match = false if tags[key.to_s] != value
    end
    match
  end

  def remove_missing_parameters(params_to_have, parameters)
    array = []
    parameters.each do |hash|
      key = hash[:parameter_key]
      key = hash["parameter_key"] if key.nil?
      if params_to_have[key.to_s] || params_to_have[key.to_sym]
        array.push hash
      else
        puts "#{Time.now.utc}: [INFO] - Ignoring extraneous parameter: #{key}"
      end
    end
    array
  end

  def reverse_dependencies
    hash = {}
    dependencies.each do |key, values|
      values.each do |name|
        name = name.to_sym
        if hash[name].nil?
          hash[name] = []
        end
        hash[name] = hash[name].concat [key.to_s]
      end
    end
    hash
  end

  def reverse_dependencies_for_stack(name)
    reverse_dependencies[name.to_sym]
  end

  def search_tags(name, app_name = nil)
    # todo: need to normalize the naming convension between layers and
    # application stacks.
    tags = {
      environment: environment,
      name:        name
    }
    tags[:appname] = app_name unless app_name.nil?
    tags
  end

  def stack_exists?(stack)
    begin
      stack.stack_id
      true
    rescue Exception => e
      false
    end
  end

  def stack_name(name = "stack")
    "#{environment}-#{layer_name(name)}-#{version_string}"
  end

  def stack_outputs(stack)
    stack.outputs.map do |output|
      {
        parameter_key:   output.output_key,
        parameter_value: output.output_value
      }
    end
  end

  def stack_parameters_from_options
    array = []
    OptionsHandler.parameter_mapping.each do |key, hash|
      parameter_key   = hash[:parameter_key]
      parameter_value = options[key]
      if key == :db_base_name
        parameter_value = [
          "Base",
          environment.capitalize,
          version.split(".").join("")
        ].join ""
      elsif key == :version_string
        parameter_value = version_string
      end
      if parameter_key && parameter_value
        if parameter_value.class == Array
          parameter_value = parameter_value.join ","
        end
        array.push({
          parameter_key:   parameter_key,
          parameter_value: parameter_value
        })
      end
    end
    array
  end

  def stack_tags(name)
    tags = [
      { key: "Environment", value: environment },
      { key: "Name", value: name },
      { key: "Version", value: version }
    ]

    if name == "application"
      tags << { key: "AppName", value: app_name }
    end

    tags
  end

  def stacks(opts = {})
    service_resource.stacks opts
  end

  def zone_name # application zone name
    "#{options[:environment]}.#{options[:regions][0]}.#{options[:domain_name]}"
  end

end
