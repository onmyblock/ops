require_relative "cloudformation_service"

class DeleteService < CloudformationService
  def delete(name = nil, version = nil, layer = nil)
    name = "network" if name.nil?
    delete_stack_with_reverse_dependencies({
      appname: name,
      reverse_dependencies: reverse_dependencies_for_stack(name),
      version: version,
      layer: layer
    })
  end

  private

  def delete_stacks(opts)
    success = true
    tags    = search_tags(opts[:layer], opts[:appname])
    tags[:version] = opts[:version] if opts[:version]
    stacks = find_stacks_with_tags tags
    stacks.each do |stack|
      begin
        stack.delete
        tail_log stack, success_statuses, failed_statuses
      rescue Exception => e
        # This will delete but return does not exist because of tail_log
        unless e.message.include? "does not exist"
          puts "Error: #{e}"
          success = false
        end
      end
    end
    success
  end

  def delete_stack_with_reverse_dependencies(opts)
    if opts[:reverse_dependencies]
      opts[:reverse_dependencies].each do |dependency|
        dependency_stack = find_stack_with_tags search_tags(dependency)
        if stack_exists? dependency_stack
          delete_stack_with_reverse_dependencies({
            name: dependency,
            reverse_dependencies: reverse_dependencies_for_stack(dependency),
            version: opts[:version]
          })
        end
      end
    end
    delete_stacks opts
  end

  def failed_statuses
    [
      "CREATE_COMPLETE",
      "CREATE_FAILED",
      "DELETE_FAILED",
      "ROLLBACK_COMPLETE",
      "ROLLBACK_FAILED",
      "UPDATE_COMPLETE",
      "UPDATE_ROLLBACK_COMPLETE",
      "UPDATE_ROLLBACK_FAILED"
    ]
  end

  def success_statuses
    %w(DELETE_COMPLETE)
  end
end
