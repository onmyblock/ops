require_relative "load_balancer_service"
require_relative "service"

class AutoscalingService < Service
  def initialize(opts = {})
    super
    configure_aws_sdk
  end

  def autoscaling_client_base(region = regions.first)
    @autoscaling_client_base ||= Aws::AutoScaling::Client.new(
      credentials: credentials,
      region:      region
    )
  end

  def autoscaling_group_subnet_ids(autoscaling_group)
    autoscaling_group.subnets.map { |s| s.subnet_id }.join(",")
  end

  def autoscaling_object
    @autoscaling_object ||= AWS::AutoScaling.new
  end

  def create_updated_autoscaling_group(group, launch_config)
    name = "#{group.name}-updated"
    autoscaling_client_base.create_auto_scaling_group(
      auto_scaling_group_name:   name,
      availability_zones:        group.availability_zone_names,
      default_cooldown:          group.default_cooldown,
      desired_capacity:          group.desired_capacity,
      health_check_grace_period: group.health_check_grace_period,
      launch_configuration_name: launch_config.name,
      load_balancer_names:       [load_balancer.name],
      max_size: group.max_size,
      min_size: group.min_size,
      tags: [
        {
          key:   "Environment",
          value: environment,
          propagate_at_launch: true
        },
        {
          key:   "Name",
          value: "base-#{environment}-#{version_string}",
          propagate_at_launch: true
        },
        {
          key:   "updated",
          value: "true",
          propagate_at_launch: false
        },
        {
          key:   "Version",
          value: version,
          propagate_at_launch: true
        }
      ],
      vpc_zone_identifier: autoscaling_group_subnet_ids(group)
    )
    autoscaling_object.groups[name]
  end

  def create_updated_launch_configuration(launch_config)
    name = "#{launch_config.name}-updated"
    autoscaling_client_base.create_launch_configuration(
      associate_public_ip_address: launch_config.associate_public_ip_address,
      image_id: instance_ami_id,
      instance_monitoring: {
        enabled: launch_config.detailed_instance_monitoring
      },
      instance_type: launch_config.instance_type,
      key_name:      launch_config.key_name,
      launch_configuration_name: name,
      security_groups: launch_configuration_security_groups(launch_config)
    )
    autoscaling_object.launch_configurations[name]
  end

  def create_updated_set
    launch_config = nil
    begin
      group         = find_autoscaling_group
      launch_config = create_updated_launch_configuration(
        launch_configuration(group)
      )
      create_updated_autoscaling_group group, launch_config
    rescue Exception => e
      launch_config.delete if launch_config
      puts e
      exit 1
    end
  end

  def delete_autoscaling_group(autoscaling_group, wait_for_instance = true)
    if wait_for_instance
      autoscaling_group.delete
    else
      autoscaling_group.delete!
    end
  end

  def delete_launch_configuration(launch_config)
    launch_config.delete
  end

  def delete_set(autoscaling_group)
    delete_autoscaling_group autoscaling_group, false
    delete_launch_configuration autoscaling_group.launch_configuration
  end

  def find_autoscaling_group(updated = false)
    autoscaling_group = nil
    autoscaling_object.groups.to_a.each do |group|
      if autoscaling_group.nil?
        correct_environment = false
        correct_updated     = false
        correct_version     = false
        group.tags.each do |tag|
          if tag[:key].downcase == "environment" && tag[:value] == environment
            correct_environment = true
          end
          if tag[:key].downcase == "updated" && tag[:value] == "true"
            correct_updated = true
          end
          if tag[:key].downcase == "version" && tag[:value] == version
            correct_version = true
          end
        end
        if correct_environment && correct_version
          if (updated && correct_updated) || (!updated && !correct_updated)
            autoscaling_group = group
          end
        end
      end
    end
    autoscaling_group
  end

  def launch_configuration(autoscaling_group)
    autoscaling_group.launch_configuration
  end

  def launch_configuration_security_groups(launch_configuration)
    launch_configuration.security_groups.map { |sg| sg.security_group_id }
  end

  private

  def load_balancer
    @load_balancer ||= load_balancer_service.find_load_balancer
  end

  def load_balancer_service
    @load_balancer_service ||= LoadBalancerService.new options
  end
end

# ruby aws/cloudformation_stack.rb -a create_autoscaling -e dev -i ami-98aa1cf0 -r us-east-1 -v 1.0.0
# ruby aws/cloudformation_stack.rb -a delete_autoscaling -e dev -r us-east-1 -v 1.0.0
