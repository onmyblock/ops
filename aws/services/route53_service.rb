require_relative "service"

class Route53Service < Service
  def initialize(opts = {})
    super
    @service = Aws::Route53
  end

  def find_hosted_zone(name)
    hosted_zones.detect { |hz| hz.name == name }
  end

  def find_record_set(hosted_zone, name)
    record_sets(hosted_zone).select { |rs| rs.name == name }
  end

  def get_change(change_info_id)
    service_client.get_change(id: change_info_id).change_info
  end

  def change_info_status(change_info)
    change_info.status
  end

  def create_hosted_zone(zone_name)
    service_client.create_hosted_zone(
      name: zone_name,
      caller_reference: "#{Time.now.to_f}"
      )
  end

  def hosted_zones
    service_client.list_hosted_zones.hosted_zones
  end

  def record_sets(hosted_zone)
    service_client.list_resource_record_sets(
      hosted_zone_id: hosted_zone.id
    ).resource_record_sets
  end

  def set_stack_weight(hosted_zone, app_fqdn, version, weight, ttl)
    response = false
    results = find_record_set(hosted_zone, app_fqdn)
    results.each do |element|
      if (element.name == app_fqdn) && (element.set_identifier == version)
        target_rrset = element.resource_records.map(&:value).join(" ")
        response = change_resource_record_set(hosted_zone, target_rrset, app_fqdn, "UPSERT", "CNAME", version, weight, ttl)
        return response
      end
    end
    return response
  end

  def change_resource_record_set(hosted_zone, target_rrset, app_fqdn, 
    action, rrset_type, set_identifier, weight, ttl = 300)
    begin
      service_client.change_resource_record_sets(
        hosted_zone_id: hosted_zone.id,
        change_batch: {
          changes: [
            {
              action: action,
              resource_record_set: {
                name: app_fqdn,
                type: rrset_type,
                set_identifier: set_identifier.to_s,
                weight: weight,
                ttl: ttl,
                resource_records: [
                  {
                    value: target_rrset,
                  },
                ],
              },
            },
          ],
        },
      )
    rescue Exception => e
      false
    end
  end

  def wait_until_change_insync(change_info)
    condition = false
    time_limit = Time.now + (60 * 5)
    while !condition && Time.now < time_limit
      sleep 10
      change    = get_change change_info.id
      condition = change_info_status(change) == "INSYNC"
      puts "#{Time.now}: record set status insync = #{condition}"
    end
    condition
  end

  def rrset_pointer_exists?(hosted_zone, app_fqdn, rrset, version)
    found = false
    results = find_record_set(hosted_zone, app_fqdn)
    results.each do |element|
      if (element.name == app_fqdn) and (element.set_identifier == version)
        return true
      end
    end
    return found
  end

  def rrset_active?(hosted_zone, app_fqdn, version)
    response = find_record_set(hosted_zone, app_fqdn)
    response.each do |rr|
      next unless rr.set_identifier == version
      if rr.weight.nil?
        return false
      elsif rr.weight > 0
        return true
      else
        return false
      end
    end
    return false
  end
end
