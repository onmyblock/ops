require "json"

class TemplateService < Service
  def add_description(description, json)
    json["Description"] = description
  end

  def pretty_generate(hash)
    JSON.pretty_generate hash
  end

  # Templates
  def application_stack_template
    stack_template "application"
  end

  def docker_registry_template
    stack_template "docker_registry"
  end

  def etcd_master_stack_template
    stack_template "etcd_master"
  end

  def jenkins_stack_template
    stack_template "jenkins"
  end

  def network_stack_template
    stack_template "network"
  end

  def shared_stack_template
    stack_template "shared"
  end

  def storage_stack_template
    stack_template "storage"
  end

  private

  def add_templates_from_stack(stack_name, template)
    file_names.each do |name|
      template_key = name.capitalize
      hash = template[template_key]
      hash = {} if hash.nil?
      json = json_from_file name, "#{stacks_folder}/#{stack_name}"
      json.each do |key, value|
        hash[key] = value
      end
      template[template_key] = hash
    end
    template
  end

  def assemble_shared_templates
    template = template_json
    file_names.each do |name|
      template[name.capitalize] = json_from_file name, shared_folder
    end
    template
  end

  def json_from_file(file_name, folder = folder)
    string = string_from_json_file file_name, template_dir, folder
    string = "{}" if string.length == 0
    JSON.parse string
  end

  def shared_folder
    "#{folder}/shared"
  end

  def stack_template(stack_name)
    add_templates_from_stack stack_name, assemble_shared_templates
  end

  def stacks_folder
    "#{folder}/stacks"
  end

  def template_json
    json_from_file "template"
  end
end
