require_relative "cloudformation_service"

class UpdateService < CloudformationService
  def update_stack(name)
    stack = find_stack_with_tags(
      search_tags(name).merge({ version: version })
    )
    if stack_exists? stack
      parameters = parameters_from_stack stack
      check_new_environment
      check_new_version

      parameters = update_parameters_with_options parameters, options
      parameters_array = array_from_parameters parameters

      begin
        response = service_client.update_stack(
          parameters: parameters_array,
          stack_name: stack.name,
          use_previous_template: true
        )
        tail_log stack, success_statuses, failed_statuses
      rescue Exception => e
        puts "Error: #{e}"
        false
      end
    else
      puts "Stack #{stack_name(name)} does not exist"
      false
    end
  end

  private

  def array_from_parameters(parameters)
    array = []
    parameters.each do |key, value|
      array.push({
        parameter_key:   key,
        parameter_value: value.to_s
      })
    end
    array
  end

  def check_new_environment
    if environment_new && environment_new != environment
      options[:environment] = environment_new
    end
  end

  def check_new_version
    if version_new && version_new != version
      options[:version]        = version_new
      options[:version_string] = version_new.split(".").join("-")
    end
  end

  def failed_statuses
    [
      "DELETE_COMPLETE",
      "DELETE_FAILED",
      "ROLLBACK_COMPLETE",
      "ROLLBACK_FAILED",
      "UPDATE_ROLLBACK_COMPLETE",
      "UPDATE_ROLLBACK_FAILED"
    ]
  end

  def parameters_from_stack(stack)
    parameters = {}
    stack.parameters.sort_by { |param| param.parameter_key }.each do |param|
      parameters[param.parameter_key.to_sym] = param.parameter_value
    end
    parameters
  end

  def print_parameters(parameters)
    # Visually see what parameters are being used in the update
    parameters.each_with_index do |v, index|
      puts "#{index}: #{v}"
    end
  end

  def success_statuses
    %w(UPDATE_COMPLETE)
  end

  def update_parameters_with_options(parameters, opts)
    opts.each do |key, value|
      hash = OptionsHandler.parameter_mapping[key]
      if hash
        parameter_key = hash[:parameter_key]
        if parameter_key
          if value.class == Array
            value = value.join ","
          end
          parameters[parameter_key.to_sym] = value
        end
      end
    end
    parameters
  end
end
