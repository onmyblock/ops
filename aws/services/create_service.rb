require "net/http"
require_relative "cloudformation_service"

class CreateService < CloudformationService
  NEW_DISCOVERY_URL = "https://discovery.etcd.io/new"

  def create(name = nil, parameters = nil, service = nil, tags = nil)
    name       = "application" if name.nil?
    parameters = stack_parameters_from_options if parameters.nil?
    create_stack_with_dependencies({
      name:         name,
      dependencies: dependencies_for_stack(name),
      parameters:   parameters,
      service:      service,
      tags:         tags
    })
  end

  private

  def add_app_name_parameter(parameters)
    parameters.concat([
      {
        parameter_key:   "AppName",
        parameter_value: app_name
      }
    ])
  end

  def add_layer_name_parameter(parameters, name)
    parameters.concat([
      {
        parameter_key: "LayerName",
        parameter_value: layer_name(name)
      }
    ])
  end

  def add_discovery_url_parameter(parameters)
    parameters.concat([
      {
        parameter_key:   "CoreOsDiscoveryURL",
        parameter_value: Net::HTTP.get(URI.parse(NEW_DISCOVERY_URL))
      }
    ])
  end

  def create_stack(opts)
    success = false
    regions.each do |region|
      begin
        validate_template opts[:template]
        opts[:tags] = stack_tags opts[:name] if opts[:tags].nil?
        stack   = resource_create opts
        success = tail_log stack, success_statuses, failed_statuses
      rescue Exception => e
        puts "Error: #{e}"
        success = false
      end
    end
    success
  end

  def create_stack_with_dependencies(opts)
    opts[:dependencies].each do |dependency|
      dependency_stack = find_stack_with_tags search_tags(dependency)
      unless stack_exists? dependency_stack
        create_stack_with_dependencies(
          name:         dependency,
          dependencies: dependencies_for_stack(dependency),
          parameters:   opts[:parameters],
          service:      opts[:service],
          tags:         opts[:tags]
        )
      end
    end
    create_stack_from_outputs opts
  end

  def create_stack_from_outputs(opts)
    outputs       = outputs_from_dependencies opts[:dependencies]
    template      = formatted_template_for_stack opts[:name], opts[:service]
    template_json = JSON.parse template

    template_parameters = template_json["Parameters"]
    combined_parameters = opts[:parameters].concat outputs
    if template_parameters["AppName"]
      combined_parameters = add_app_name_parameter(combined_parameters)
    end
    if template_parameters["CoreOsDiscoveryURL"]
      combined_parameters = add_discovery_url_parameter(combined_parameters)
    end
    if template_parameters["LayerName"]
      combined_parameters = add_layer_name_parameter(combined_parameters, opts[:name])
    end
    parameters = remove_missing_parameters(
      template_parameters, combined_parameters
    )
    opts[:parameters] = parameters
    opts[:template]   = template
    create_stack opts
  end

  def failed_statuses
    [
      "CREATE_FAILED",
      "DELETE_COMPLETE",
      "DELETE_FAILED",
      "ROLLBACK_COMPLETE",
      "ROLLBACK_FAILED",
      "UPDATE_COMPLETE",
      "UPDATE_ROLLBACK_COMPLETE",
      "UPDATE_ROLLBACK_FAILED"
    ]
  end

  def formatted_template_for_stack(name, service)
    service.pretty_generate template_for_stack(name, service)
  end

  def outputs_from_dependencies(dependencies)
    outputs = []
    dependencies.each do |dependency|
      dependency_stack = find_stack_with_tags search_tags(dependency)
      outputs.concat stack_outputs(dependency_stack)
    end
    outputs
  end

  def resource_create(opts)
    service_resource.create_stack(
      parameters:    opts[:parameters],
      stack_name:    stack_name(opts[:name]),
      tags:          opts[:tags],
      template_body: opts[:template]
    )
  end

  def success_statuses
    %w(CREATE_COMPLETE)
  end

  def template_for_stack(name, service)
    {
      application:     service.application_stack_template,
      docker_registry: service.docker_registry_template,
      etcd_master:     service.etcd_master_stack_template,
      jenkins:         service.jenkins_stack_template,
      network:         service.network_stack_template,
      shared:          service.shared_stack_template,
      storage:         service.storage_stack_template
    }[name.to_sym]
  end

  def validate_template(json)
    service_client.validate_template template_body: json
  end
end
