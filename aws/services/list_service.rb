require_relative "cloudformation_service"

class ListService < CloudformationService
  def list_stacks
    puts "\n"
    puts "-" * 60
    regions.each do |region|
      puts "#{region}:"
      puts "-" * 40
      service_resource(region).stacks.each do |stack|
        array = [
          stack.stack_status
        ]
        puts "#{stack.stack_name}: #{array.join(" - ")}"
      end
    end
    puts "-" * 60
    true
  end
end
