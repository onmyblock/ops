require_relative "service"

class LoadBalancerService < Service
  def initialize(opts = {})
    super
    @service = Aws::ElasticLoadBalancing
  end

  def dns_name_from_outputs(outputs) # ElasticLoadBalancerBaseDNSName
    output = outputs.detect do |o|
      o[:output_key] == "ElasticLoadBalancerBaseDNSName"
    end

    output[:output_value]
  end

  def name_from_outputs(outputs) # ElasticLoadBalancerBaseName
    op = outputs.detect { |o| o[:output_key] == "ElasticLoadBalancerBaseName" }
    op[:output_value]
  end

  def wait_until_instances_in_service(name)
    condition  = false
    time_limit = Time.now + (60 * 10)
    while !condition && Time.now < time_limit
      sleep 10
      condition = check_instance_health instance_states(name)
      puts "#{Time.now}: all instances in service = #{condition}"
    end
    condition
  end

  private

  def check_instance_health(states)
    all_in_service = true
    states.each do |state|
      break unless all_in_service
      all_in_service = false if state.state != "InService"
    end
    all_in_service
  end

  def instance_states(name)
    service_client.describe_instance_health(
      load_balancer_name: name
    ).instance_states
  end
end
