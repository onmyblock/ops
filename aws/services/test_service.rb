require "net/https"
require "uri"
require_relative "service"

class TestService < Service
  def initialize(opts = {})
    super
  end

  def heartbeat(domain_name)
    uri = URI.parse("http://#{domain_name}/health")
    Net::HTTP.get_response(uri).kind_of?(Net::HTTPSuccess)
  end
end
