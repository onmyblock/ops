require "aws-sdk"
require "aws-sdk-v1"

class Service
  def initialize(opts = {})
    @options           = opts
    @service           = nil
    @service_clients   = {}
    @service_resources = {}
  end

  def credentials
    @credentials ||= Aws::Credentials.new(
      ENV["AWS_ACCESS_KEY_ID"], ENV["AWS_SECRET_ACCESS_KEY"]
    )
  end

  def options
    @options
  end

  def service_client(region = regions.first)
    client = @service_clients[region]
    if client.nil?
      client = @service::Client.new credentials: credentials, region: region
      @service_clients[region] = client
    end
    client
  end

  def service_resource(region = regions.first)
    resource = @service_resources[region]
    if resource.nil?
      resource = @service::Resource.new client: service_client(region)
      @service_resources[region] = resource
    end
    resource
  end

  def string_from_json_file(file_name, template_dir, folder)
    path = File.expand_path(
      "#{template_dir}/#{folder}/#{file_name}.json"
    )
    if File.exists? path
      file = File.open path
      data = file.read
      file.close
      data.to_str
    else
      puts "#{Time.now.utc}: [INFO] - Not found: #{path}"
      ""
    end
  end

  def tail_log(stack, success_statuses, failed_statuses)
    event_ids = []
    name      = stack.name
    status    = stack.stack_status
    while !success_statuses.include? status
      sleep 10
      stack  = find_stack name
      status = stack.stack_status
      events = stack.events.to_a.sort { |x,y| x.timestamp <=> y.timestamp }
      events.each do |event|
        unless event_ids.include? event.id
          array_string = [
            event.resource_status,
            event.resource_type,
            event.logical_resource_id,
            event.resource_status_reason
          ].join " - "
          puts "#{event.timestamp}: #{array_string}"
          event_ids.push event.id
        end
      end
    end
    !failed_statuses.include? status
  end

  def version_string
    version.split(".").join("-")
  end

  private

  def configure_aws_sdk(region = regions.first)
    AWS.config(
      access_key_id:     ENV["AWS_ACCESS_KEY_ID"],
      secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"],
      region:            region
    )
  end

  def environment
    options[:environment]
  end

  def environment_new
    options[:environment_new]
  end

  def file_names
    %w(parameters mappings conditions resources outputs)
  end

  def folder
    options[:folder]
  end

  def instance_ami_id
    options[:instance_ami_id]
  end

  def regions
    options[:regions]
  end

  def template_dir
    options[:template_dir]
  end

  def version
    options[:version]
  end

  def version_new
    options[:version_new]
  end
end
