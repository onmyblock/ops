require_relative "load_balancer_service"
require_relative "update_service"

class UpdateProcessService < UpdateService
  def update_stack(name)
    environment_old = environment
    version_old     = version

    # 1. create new <layer> stack using existing parameters from the old stack
    created = clone_stack_with_updates name, environment_old, version_old

    # 2. make sure all the instances are up and running
    in_service = true # Uncomment below when instances are setup to work!!!
    # in_service = created ? check_instances(load_balancer_name(name)) : false

    # 3. run all tests and be able to return a response
    passes = in_service ? run_tests : false

    # 4. run script to re-route traffic to new <layer> stack
    routed_traffic_success = false
    if passes
      # curl the elastic load balancer dns name before re-routing traffic
      routed_traffic_success = route_traffic name
    else
      delete_stack name, version
    end

    # 5. delete old <layer> stack
    delete_stack name, version_old if routed_traffic_success
  end

  private

  def clone_stack_with_updates(name, environment_old, version_old)
    old_stack = find_stack_with_tags(
      search_tags(name).merge({ version: version })
    )
    if stack_exists? old_stack
      parameters = parameters_from_stack old_stack
      
      check_new_environment
      check_new_version

      parameters = update_parameters_with_options parameters, options

      parameters_array = array_from_parameters parameters

      if options[:folder].nil?
        options[:folder] = 
          OptionsHandler.parameter_mapping[:folder][:default_value]
      end
      if options[:template_dir].nil?
        options[:template_dir] =
          OptionsHandler.parameter_mapping[:template_dir][:default_value]
      end

      create_service   = CreateService.new options
      template_service = TemplateService.new options
      tags = stack_tags(name)
      tags.push({ key: "EnvironmentOld", value: environment_old })
      tags.push({ key: "VersionOld", value: version_old })
      create_service.create name, parameters_array, template_service, tags
    else
      puts "Stack #{stack_name(name)} does not exist"
      false
    end
  end

  def check_instances(name)
    load_balancer_service.wait_until_instances_in_service name
  end

  def delete_stack(name, ver)
    DeleteService.new(options).delete name, ver
  end

  def load_balancer_name(name)
    stack = find_stack_with_tags search_tags(name)
    load_balancer_service.name_from_outputs stack_outputs(stack)
  end

  def load_balancer_service
    @load_balancer_service ||= LoadBalancerService.new options
  end

  def route53_service
    @route53_service ||= Route53Service.new options
  end

  def route_traffic(name)
    stack = find_stack_with_tags(
      search_tags(name).merge({ version: version })
    )
    output = stack_outputs(stack).detect do |output|
      output[:parameter_key] == "ElasticLoadBalancerBaseDNSName"
    end
    if output
      name        = "onmyblockphotography.com."
      hosted_zone = route53_service.find_hosted_zone name
      record_set  = route53_service.find_record_set hosted_zone, name
      response    = route53_service.update_record_set_dns_name(
        hosted_zone, record_set, output[:parameter_value]
      )
      change_info_id = response.change_info.id
      change_info    = route53_service.get_change change_info_id
      route53_service.wait_until_change_insync change_info
    else
      false
    end
  end

  def run_tests
    puts "Running tests..."
    true
  end
end
