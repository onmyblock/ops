require_relative "objects/all"
require_relative "services/all"

options_handler = OptionsHandler.new
options_handler.convert_options

options = options_handler.options
action  = options[:action]
layer   = options[:layer]

options_handler.set_default_options if action == "create"
options_handler.validate_options
options_handler.log_options


success = false
case action
when "create"
  template_service = TemplateService.new(options)
  success = CreateService.new(options).create layer, nil, template_service
when "delete"
  success = DeleteService.new(options).delete layer, options[:version]
when "list"
  success = ListService.new(options).list_stacks
when "update"
  # success = UpdateService.new(options).update_stack layer
  success = UpdateProcessService.new(options).update_stack layer
end
exit success ? 0 : 1

# Sample
# --------------------
# Create
# ----------
# ruby aws/cloudformation_stack.rb -a create -z us-east-1a -k aws_omb_us_east_1 -n ami-224dc94a -r us-east-1 --docker_application_image_tag=dangerous/rails-dev-postgres:production -v 1.0.0 -e dev -l network
# --------------------
# Delete
# ----------
# ruby aws/cloudformation_stack.rb -a delete -r us-east-1 -e dev --layer application
# --------------------
# List
# ----------
# ruby aws/cloudformation_stack.rb -a list -r us-east-1
# --------------------
# Update
# ----------
# ruby aws/cloudformation_stack.rb -a update -e dev -r us-east-1 -l application -v 1.0.0 --version_new=1.0.1
# ruby aws/cloudformation_stack.rb -a create -z us-east-1a -k aws_omb_us_east_1 -n ami-224dc94a -r us-east-1 --docker_application_image_tag=dangerous/rails-dev-postgres:production -v 1.0.0 -e dev -l etcd_master
