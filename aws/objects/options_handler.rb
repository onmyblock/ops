require "getoptlong"

class OptionsHandler
  def initialize
    @options = {}
  end

  def self.parameter_mapping
    {
      action: {
        default_value: nil,
        parameter_key: nil
      },
      asg_max_size: {
        default_value: "4",
        parameter_key: "AutoScalingGroupMaxSize"
      },
      asg_min_size: {
        default_value: "1",
        parameter_key: "AutoScalingGroupMinSize"
      },
      app_name: {
        default_value: nil,
      },
      availability_zones: {
        default_value: ["us-west-1a"],
        parameter_key: "AvailabilityZones"
      },
      aws_access_key_id: {
        default_value: nil,
        parameter_key: "AwsAccessKeyId"
      },
      aws_secret_access_key: {
        default_value: nil,
        parameter_key: "AwsSecretAccessKey"
      },
      cidr_block: {
        default_value: "192.168.0.0/16",
        parameter_key: "VpcCidrBlock"
      },
      db_base_allocated_storage: {
        default_value: "5",
        parameter_key: "DBInstanceBaseAllocatedStorage"
      },
      db_base_class: {
        default_value: "db.m3.medium",
        parameter_key: "DBInstanceBaseClass"
      },
      db_base_create: {
        default_value: "true",
        parameter_key: "DBInstanceBaseCreate"
      },
      db_base_deletion_policy: {
        default_value: "Delete",
        parameter_key: "DBInstanceBaseDeletionPolicy"
      },
      db_base_name: {
        default_value: nil,
        parameter_key: "DBInstanceBaseName"
      },
      db_base_username: {
        default_value: "postgres",
        parameter_key: "DBInstanceBaseMasterUsername"
      },
      db_base_password: {
        default_value: "postgres",
        parameter_key: "DBInstanceBaseMasterPassword"
      },
      db_base_port: {
        default_value: "5432",
        parameter_key: "DBInstanceBasePort"
      },
      db_replication: {
        default_value: "false",
        parameter_key: "DBInstanceReplication"
      },
      docker_application_image_tag: {
        default_value: nil,
        parameter_key: "DockerApplicationImageTag"
      },
      docker_registry_dns_prefix: {
        default_value: "docker",
        parameter_key: "DockerRegistryDnsPrefix"
      },
      docker_registry_dns_zone: {
        default_value: "onmyblock.com",
        parameter_key: "DockerRegistryDnsZone"
      },
      docker_registry_elb_ssl: {
        default_value: nil,
        parameter_key: "DockerRegistryElasticLoadBalancerSslCertificate"
      },
      docker_registry_image: {
        default_value: "registry:0.9.0",
        parameter_key: "DockerRegistryImage"
      },
      docker_registry_passwords: {
        default_value: nil,
        parameter_key: "DockerRegistryPasswords"
      },
      docker_registry_s3_bucket: {
        default_value: "omb-docker-registry",
        parameter_key: "DockerRegistryS3BucketName"
      },
      domain_name: {
        default_value: "onmyblock.com"
      },
      environment: {
        default_value: "dev",
        parameter_key: "Environment"
      },
      environment_new: {
        default_value: nil,
        parameter_key: nil
      },
      folder: {
        default_value: "templates",
        parameter_key: nil
      },
      instance_ami_id: {
        default_value: "ami-076e6542",
        parameter_key: "InstanceAmiId"
      },
      instance_ami_version: {
        default_value: "1.0.0",
        parameter_key: "InstanceAmiVersion"
      },
      instance_base_role: {
        default_value: "web",
        parameter_key: "InstanceBaseRole"
      },
      instance_count: {
        default_value: "2",
        parameter_key: "InstanceCount"
      },
      instance_type_base: {
        default_value: "c3.large",
        parameter_key: "InstanceTypeBase"
      },
      jenkins_security_group: {
        default_value: nil,
        parameter_key: "JenkinsSecurityGroup"
      },
      jenkins_subnet: {
        default_value: nil,
        parameter_key: "JenkinsSubnet"
      },
      key_name: {
        default_value: "aws_omb_us_west_1",
        parameter_key: "KeyName"
      },
      layer: {
        default_value: nil,
      },
      nat_ami_id: {
        default_value: "ami-030f4133",
        parameter_key: "NatAmiId"
      },
      peer_vpc_cidr_blocks: {
        default_value: nil,
        parameter_key: "PeerVpcCidrBlocks"
      },
      peer_vpc_ids: {
        default_value: nil,
        parameter_key: "PeerVpcIds"
      },
      regions: {
        default_value: nil,
        parameter_key: nil
      },
      template_dir: {
        default_value: "aws",
        parameter_key: nil
      },
      version: {
        default_value: nil,
        parameter_key: "Version"
      },
      version_new: {
        default_value: nil,
        parameter_key: nil
      },
      version_old: {
        default_value: nil,
        parameter_key: nil
      },
      version_string: {
        default_value: nil,
        parameter_key: "VersionString"
      }
    }
  end

  def convert_options
    extract_options.each do |opt, arg|
      key_symbol = opt.gsub("--", "").to_sym
      arrays = [
        :availability_zones, :peer_vpc_cidr_blocks, :peer_vpc_ids, :regions
      ]
      if arrays.include? key_symbol
        arg = arg.split ","
      end
      capitalize = [
        :db_base_deletion_policy,
        :elasticache_deletion_policy
      ]
      if capitalize.include? key_symbol
        arg = arg.capitalize
      end
      options[key_symbol] = arg
    end
  end

  def extract_options
    GetoptLong.new(
      ["--action", "-a", GetoptLong::OPTIONAL_ARGUMENT],
      ["--app_url", GetoptLong::OPTIONAL_ARGUMENT],
      ["--asg_max_size", GetoptLong::OPTIONAL_ARGUMENT],
      ["--asg_min_size", GetoptLong::OPTIONAL_ARGUMENT],
      ["--app_name", GetoptLong::REQUIRED_ARGUMENT],
      ["--availability_zones", "-z", GetoptLong::OPTIONAL_ARGUMENT],
      ["--aws_access_key_id", GetoptLong::OPTIONAL_ARGUMENT],
      ["--aws_secret_access_key", GetoptLong::OPTIONAL_ARGUMENT],
      ["--cidr_block", "-c", GetoptLong::OPTIONAL_ARGUMENT],
      ["--db_base_allocated_storage", GetoptLong::OPTIONAL_ARGUMENT],
      ["--db_base_class", GetoptLong::OPTIONAL_ARGUMENT],
      ["--db_base_create", GetoptLong::OPTIONAL_ARGUMENT],
      ["--db_base_deletion_policy", GetoptLong::OPTIONAL_ARGUMENT],
      ["--db_base_username", GetoptLong::OPTIONAL_ARGUMENT],
      ["--db_base_password", GetoptLong::OPTIONAL_ARGUMENT],
      ["--db_base_port", GetoptLong::OPTIONAL_ARGUMENT],
      ["--db_replication", GetoptLong::OPTIONAL_ARGUMENT],
      ["--docker_application_image_tag", GetoptLong::OPTIONAL_ARGUMENT],
      ["--docker_registry_dns_prefix", GetoptLong::OPTIONAL_ARGUMENT],
      ["--docker_registry_dns_zone", GetoptLong::OPTIONAL_ARGUMENT],
      ["--docker_registry_elb_ssl", GetoptLong::OPTIONAL_ARGUMENT],
      ["--docker_registry_image", GetoptLong::OPTIONAL_ARGUMENT],
      ["--docker_registry_passwords", GetoptLong::OPTIONAL_ARGUMENT],
      ["--docker_registry_s3_bucket", GetoptLong::OPTIONAL_ARGUMENT],
      ["--domain_name", GetoptLong::OPTIONAL_ARGUMENT],
      ["--elb_dns_name", GetoptLong::OPTIONAL_ARGUMENT],
      ["--elb_name", GetoptLong::OPTIONAL_ARGUMENT],
      ["--environment", "-e", GetoptLong::OPTIONAL_ARGUMENT],
      ["--environment_new", GetoptLong::OPTIONAL_ARGUMENT],
      ["--folder", "-f", GetoptLong::OPTIONAL_ARGUMENT],
      ["--instance_ami_id", "-i", GetoptLong::OPTIONAL_ARGUMENT],
      ["--instance_ami_version", GetoptLong::OPTIONAL_ARGUMENT],
      ["--instance_base_role", GetoptLong::OPTIONAL_ARGUMENT],
      ["--instance_count", GetoptLong::OPTIONAL_ARGUMENT],
      ["--instance_type_base", GetoptLong::OPTIONAL_ARGUMENT],
      ["--jenkins_security_group", GetoptLong::REQUIRED_ARGUMENT],
      ["--jenkins_subnet", GetoptLong::REQUIRED_ARGUMENT],
      ["--key_name", "-k", GetoptLong::OPTIONAL_ARGUMENT],
      ["--layer", "-l", GetoptLong::OPTIONAL_ARGUMENT],
      ["--nat_ami_id", "-n", GetoptLong::OPTIONAL_ARGUMENT],
      ["--peer_vpc_cidr_blocks", GetoptLong::OPTIONAL_ARGUMENT],
      ["--peer_vpc_ids", GetoptLong::OPTIONAL_ARGUMENT],
      ["--regions", "-r", GetoptLong::OPTIONAL_ARGUMENT],
      ["--template_dir", "-t", GetoptLong::OPTIONAL_ARGUMENT],
      ["--version", "-v", GetoptLong::REQUIRED_ARGUMENT],
      ["--version_old", GetoptLong::OPTIONAL_ARGUMENT],
      ["--version_new", GetoptLong::REQUIRED_ARGUMENT]
    )
  end

  def log_options
    puts "-" * 80
    puts "Parameters"
    puts "-" * 40
    options.sort.each do |key, value|
      puts "#{key}: #{value}"
    end
  end

  def options
    @options
  end

  def set_default_options
    OptionsHandler.parameter_mapping.each do |key, hash|
      default_value = hash[:default_value]
      if default_value && options[key].nil?
        options[key] = default_value
      end
    end
  end

  def validate_options
    if options[:action].nil?
      puts "Missing argument: action"
      exit 1
    elsif !valid_actions.include? options[:action]
      puts
        "Invalid argument: action - please use #{valid_actions.join(", ")}"
      exit 1
    end
    if options[:asg_max_size].to_i < options[:asg_min_size].to_i
      puts "Invalid arguments: asg_max_size cannot be larger than asg_min_size"
      exit 1
    end
    if options[:peer_vpc_cidr_blocks].nil? && options[:peer_vpc_ids].nil?
      options[:peer_vpc_cidr_blocks] = ["0"]
      options[:peer_vpc_ids]         = ["0"]
    elsif options[:peer_vpc_cidr_blocks].nil? || options[:peer_vpc_ids].nil?
      if options[:peer_vpc_cidr_blocks].nil?
        puts "Missing argument: peer_vpc_cidr_blocks"
      else
        puts "Missing argument: peer_vpc_ids"
      end
      exit 1
    end
    if !["delete", "list"].include?(options[:action]) && options[:version].nil?
      puts "Missing argument: version"
      exit 1
    end
  end

  private

  def valid_actions
    %w(create delete list update)
  end
end
