# Jenkins

## Local installation

* git clone `git@github.com:onmyblock/omb.git` (in the jenkins project root for testing)
* Run `vagrant up` in your terminal
* Visit [http://localhost:8080/](http://localhost:8080/)

## Configuration

* Install the `ansicolor` plugin
* Install the `docker` plugin
* Install the `git` plugin
* Install the `github` plugin

## Example build job

```
#!/bin/bash -x

(docker images | grep redis | grep 2.8.9) || docker pull redis:2.8.9
(docker images | grep helmi03/docker-postgis | grep latest) || docker pull helmi03/docker-postgis:latest
(docker images | grep onmyblock/build | grep latest) || docker pull onmyblock/build:latest

redis_id=$(docker run --name "redis-$BUILD_TAG" -d redis:2.8.9)
postgis_id=$(docker run --name "db-$BUILD_TAG" -d helmi03/docker-postgis:latest)

build_id=$(docker run -d --link db-$BUILD_TAG:db --link redis-$BUILD_TAG:redis -v $WORKSPACE:/build -t onmyblock/build:latest)

# Attach to the container's streams so that we can see the output.
docker attach $build_id

# As soon as the process exits, get its return value.
exit_code=$(docker wait $build_id)

docker stop $redis_id $postgis_id
docker rm $redis_id $postgis_id $build_id

exit $exit_code
```
