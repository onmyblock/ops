# Add jenkins-ci.org and docker.io apt keys to the system's trusted keys
wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
wget -qO- https://get.docker.io/gpg | sudo apt-key add -

# Add jenkins-ci.org and docker.io repositories to apt's sources list
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo sh -c "echo deb http://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"

# Install jenkins and docker, create a jenkins user, and add an init script at /etc/init.d/jenkins
sudo apt-get update
sudo apt-get -y install jenkins docker.io # lxc-docker
sudo apt-get clean

# Start Jenkins
sudo service jenkins start
