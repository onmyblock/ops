#!/usr/bin/env ruby

require 'aws-sdk'
require 'aws-sdk-v1'
require 'optparse'

options = {:access_key_id => nil, :secret_access_key => nil}

parser = OptionParser.new do|opts|
	opts.banner = "Usage: get_ami_id.rb [options]"
	opts.on('-k', '--access_key_id access_key_id', 'access_key_id') { |access_key_id| options[:access_key_id] = access_key_id;}
	opts.on('-s', '--secret_access_key secret_access_key', 'secret_access_key') { |secret_access_key| options[:secret_access_key] = secret_access_key;}
	opts.on('-r', '--region region', 'region') { |region| options[:region] = region;}
	opts.on('-n', '--name name', 'name') { |name| options[:name] = name;}

	opts.on('-h', '--help', 'Displays Help') do
		puts opts
		exit
	end
end
parser.parse!
exit(1) unless options[:access_key_id] && options[:secret_access_key] && options[:name]
AWS.config(:access_key_id => options[:access_key_id], :secret_access_key => options[:secret_access_key] )

ec2 = AWS::EC2.new(region: options[:region]) 
puts ec2.images.filter("name",options[:name]).map(&:id).shift.strip


