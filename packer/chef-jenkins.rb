current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "jenkins"
client_key               "/Users/Ram/gitrepos/omb/chef/.chef/jenkins.pem"
chef_server_url          "https://54.68.206.123/organizations/omb"
cache_type               'BasicFile'
cache_options            ( :path => "#{ENV['HOME']}/.chef/checksums" )
cookbook_path            ["#{current_dir}/cookbooks", "#{current_dir}/site-cookbooks"]
role_path                ["#{current_dir}/roles"]
data_bag_path            ["#{current_dir}/data_bags"]

jenkins({
  :repo_dir => /opt/chef,
  :repo_url => 'https://github.com/onmyblock/chef.git',
  :git_user => "jenkins",
  :git_email => "jenkins@onmyblock.com",
  :env_to => "build",
  :branch => "master",
  :foodcritic => {
    :fail_tags => ["correctness"],
    :tags => [],
    :include_rules => []
  }
})