#!/bin/bash

mkdir /tmp/packer-chef-client
chmod 0755 /tmp/packer-chef-client

sudo mkdir -p /etc/chef/ohai/hints
sudo touch /etc/chef/ohai/hints/ec2.json

sudo mkdir -p /var/log/chef

#apt-get update && aptitude -y safe-upgrade

exit 0
